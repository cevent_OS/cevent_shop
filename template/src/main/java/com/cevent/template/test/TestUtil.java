package com.cevent.template.test;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * @author cevent
 * @create 2025/1/26 21:36
 */
public class TestUtil {
    //主包名
    static String mainPkg="cevent_server";
    //模版名称
    static String templateName="template";
    //环境配置
    static String modeName="test";
    //ftl名称
    static String ftlName="Test.ftl";
    //ftl模板路径
    static String ftlPath=templateName+ "\\src\\main\\java\\com\\cevent\\template\\"+modeName+"\\";

    //文件生成目标:目录
    static String filePath=templateName+ "\\src\\main\\java\\com\\cevent\\template\\"+modeName+"\\";
    //文件生成目标：名称
    static String fileName="TestDto.java";
    //freemarker模板引擎
    static Template templateEngine;

    //1.初始化freemarker
    public static void initConfig(String ftlName) throws IOException{
        Configuration configuration=new Configuration(Configuration.VERSION_2_3_33);
        configuration.setDirectoryForTemplateLoading(new File(ftlPath));
        configuration.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_33));

        templateEngine=configuration.getTemplate(ftlName);
    }

    /**
     * 2.生成文件
     * @param targetFileName 文件名
     * @param map
     */
    public static void generateFile(String targetFileName, Map<String,Object> map) throws IOException, TemplateException {
        //1.文件名
        FileWriter fw=new FileWriter(targetFileName);
        BufferedWriter bw=new BufferedWriter(fw);

        templateEngine.process(map,bw);
        bw.flush();
        fw.close();
    }

    //测试生成DTO
    public static void main (String [] args) throws IOException, TemplateException {
        initConfig(ftlName);
        generateFile(filePath+fileName,null);
    }
}
