package com.cevent.template.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

/**
 * freemarker模板引擎
 */
@SpringBootApplication

public class TemplateApp {
    private static final Logger LOG= LoggerFactory.getLogger(TemplateApp.class);
    public static void main(String[] args) {
        SpringApplication app=new SpringApplication(TemplateApp.class);
        Environment env=app.run(args).getEnvironment();
        LOG.info("启动成功");
        LOG.info("Template启动地址：\t http://127.0.0.1:{}",env.getProperty("server.port"));
    }
}
