package com.cevent.template.prod;

public class TestDto2 {
    private String id;
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [ ");
        sb.append("Hash = ").append(hashCode());

        sb.append(", id = ").append(id);

        sb.append(" ] ");
        return sb.toString();
    }
}
