package com.cevent.${COMMON_MODULE_NAME}.dto;

<#list FIELD_TYPE_LIST as type>
    <#if type=="Date">
        import java.util.Date;
        import com.fasterxml.jackson.annotation.JsonFormat;
    </#if>
    <#if type=="BigDecimal">
        import java.math.BigDecimal;
    </#if>
</#list>


<#assign now=.now>
<#assign formatDate=now?string("yyyy-MM-dd HH:mm:ss")>

/**
 * @author cevent
 * @create ${formatDate}
 * @description ${TABLE_NAME_CN}
 */
public class ${CLASS_NAME_BIG}Dto {

    <#list FIELD_SET_LIST as field>
    //${field.comment}
    <#if field.typeJava=="Date">
        @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    </#if>
    private ${field.typeJava} ${field.nameSmallHump};
    </#list>

    <#list FIELD_SET_LIST as field>
    public ${field.typeJava} get${field.nameBigHump}(){
        return ${field.nameSmallHump};
    }
    public void set${field.nameBigHump}(${field.typeJava} ${field.nameSmallHump}){
        this.${field.nameSmallHump}=${field.nameSmallHump};
    }
    </#list>

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [ ");
        sb.append("Hash = ").append(hashCode());

        <#list FIELD_SET_LIST as field>
            sb.append(", ${field.nameSmallHump} = ").append(${field.nameSmallHump});
        </#list>

        sb.append(" ] ");
        return sb.toString();
    }

}
