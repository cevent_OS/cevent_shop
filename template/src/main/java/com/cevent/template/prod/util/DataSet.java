package com.cevent.template.prod.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cevent
 * @create 2025/1/26 23:21
 */
public class DataSet {
    // 首字母大写
    public static String prefixBigWrite(String words) {
        String result = "";
        if (!words.isEmpty()) {
            StringBuilder sb = new StringBuilder(words);
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            result = sb.toString();
        }
        return result;
    }

    // 下划线转小驼峰
    public static String underLineToSmallHump(String words) {
        words = words.toLowerCase();
        Pattern line = Pattern.compile("_(\\w)");
        Matcher matcher = line.matcher(words);
        StringBuffer sb = new StringBuffer();
        // 寻找匹配的子串，找到第一个_之后的第1个字符，进行大写
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }

        matcher.appendTail(sb);

        return sb.toString();
    }

    // 下划线转大驼峰
    public static String underLineToBigHump(String words) {
        String result = underLineToSmallHump(words);

        return result.substring(0, 1).toUpperCase() + result.substring(1);
    }

    // 数据库类型转java类型
    public static String sqlTypeToJavaType(String sqlType) {
        String result = "";
        if (sqlType.toLowerCase().contains("varchar")
                || sqlType.toLowerCase().contains("char")
                || sqlType.toLowerCase().contains("text")) {
            result="String";
        }else if(sqlType.toLowerCase().contains("datetime")){
            result="Date";
        }else if(sqlType.toLowerCase().contains("int")){
            result="Integer";
        }else if(sqlType.toLowerCase().contains("long")){
            result="Long";
        }else if(sqlType.toLowerCase().contains("decimal")){
            result="BigDecimal";
        }else{
            result="String";
        }

        return result;
    }

    //enum枚举字段处理：enumName=ENUM_NAME
    public static String getEnumToUnderLine(String enumName){
        //section_charge_enum
        String result=transUnderLine(enumName);
        //转大写，并去除_enum
        result=result.substring(0,result.length()).toUpperCase().replace("_ENUM","");
        return result;
    }

    /**
     * 小驼峰：转 小写_下划线，第一位是_下划线
     * 如：sectionChargeEnum 转为_section_charge_enum
     */
    public static String transUnderLine(String str){
        Pattern pattern=Pattern.compile("[A-Z]");
        Matcher matcher=pattern.matcher(str);
        StringBuffer sb=new StringBuffer(str);
        if(matcher.find()){
            matcher.appendReplacement(sb,"_"+matcher.group(1).toLowerCase());
        }
        matcher.appendTail(sb);

        return sb.toString();
    }

    //获取所有java类型,set去重
    public static Set<String> getJavaTypes(List<FieldSet> fieldSetList){
        Set<String> stringSet=new HashSet<>();
        for(int i=0,l=fieldSetList.size();i<l;i++){
            FieldSet fieldSet=fieldSetList.get(i);
            stringSet.add(fieldSet.getTypeJava());
        }
        return stringSet;
    }

}
