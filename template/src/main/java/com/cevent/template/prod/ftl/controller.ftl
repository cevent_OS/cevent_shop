package com.cevent.${CMS_MODULE_NAME}.controller.admin;

import com.cevent.${COMMON_MODULE_NAME}.domain.${CLASS_NAME_BIG};
import com.cevent.${COMMON_MODULE_NAME}.dto.${CLASS_NAME_BIG}ChildrenDto;
import com.cevent.${COMMON_MODULE_NAME}.dto.${CLASS_NAME_BIG}Dto;
import com.cevent.${COMMON_MODULE_NAME}.dto.PageDto;
import com.cevent.${COMMON_MODULE_NAME}.dto.RespDto;
import com.cevent.${COMMON_MODULE_NAME}.service.${CLASS_NAME_BIG}Service;
import com.cevent.${COMMON_MODULE_NAME}.util.UuidUtil;
import com.cevent.${COMMON_MODULE_NAME}.util.ValidatorUtil;
import com.cevent.${COMMON_MODULE_NAME}.exception.ValidatorException;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

<#assign now=.now>
<#assign formatDate=now?string("yyyy-MM-dd HH:mm:ss")>

/**
   @description ${TABLE_NAME_CN}
 * @author cevent
 * @create ${formatDate}
 */
@RestController
@RequestMapping("/admin/${CLASS_NAME_SMALL}")
public class ${CLASS_NAME_BIG}Controller {

    public static final String SERVER_NAME="${TABLE_NAME_CN}";

    // 快捷键logf
    private static final Logger LOG = LoggerFactory.getLogger(${CLASS_NAME_BIG}Controller.class);

    @Resource
    private ${CLASS_NAME_BIG}Service ${CLASS_NAME_SMALL}Service;

    /**
     * 分页配置
     *
     * @param pageDto 接受表单方式
     * @param domain  配置header可以没有参数，required=false
     * @return
     */
    @PostMapping("/pageData")
    public RespDto getList(@RequestBody PageDto pageDto, @RequestHeader(value = "domain", required = false) String domain) {
        LOG.info("pageDto: {},domain: {}", pageDto, domain);
        ${CLASS_NAME_SMALL}Service.getList(pageDto, domain);
        RespDto respDto = new RespDto();
        respDto.setContent(pageDto);
        return respDto;
    }

    @RequestMapping("/tree")
    public RespDto getTree(@RequestHeader(value = "domain",required = false) String domain){
        List<${CLASS_NAME_BIG}ChildrenDto> categoryChildrenDtos=${CLASS_NAME_SMALL}Service.getTree(domain);

        RespDto respDto=new RespDto();
        if(categoryChildrenDtos.size()>0){
            respDto.setContent(categoryChildrenDtos);
        }else{
            respDto.setSuccess(false);
            respDto.setMessage("暂无分类数据，请联系后台进行维护");
        }

        return respDto;
    }

    @PostMapping("/save")
    public RespDto save(@RequestBody ${CLASS_NAME_BIG}Dto ${CLASS_NAME_SMALL}Dto, @RequestHeader(value = "domain", required = false) String domain) {
        RespDto respDto = new RespDto();

        ValidatorUtil.isEmpty(${CLASS_NAME_SMALL}Dto.getParentId(), "父级ID");
        ValidatorUtil.isEmpty(domain, "域名");
        ValidatorUtil.isEmpty(${CLASS_NAME_SMALL}Dto.getLevel(), "模块等级");
        ValidatorUtil.isEmpty(${CLASS_NAME_SMALL}Dto.getSort(), "排序");
        ValidatorUtil.isEmpty(${CLASS_NAME_SMALL}Dto.getIsShow(), "是否显示");


        ${CLASS_NAME_SMALL}Service.save(${CLASS_NAME_SMALL}Dto, domain);

        respDto.setContent(${CLASS_NAME_SMALL}Dto);
        return respDto;
    }

    @PostMapping("saveTree")
    public RespDto saveTree(@RequestBody List<${CLASS_NAME_BIG}ChildrenDto> childrenDtos,@RequestHeader(value = "domain",required = false) String domian){

        RespDto respDto=new RespDto();

        respDto.setContent(${CLASS_NAME_SMALL}Service.saveTree(childrenDtos,domian));

        return respDto;
    }

    @GetMapping("/detail/{uniId}")
    public RespDto detail(@PathVariable String uniId) {
        ${CLASS_NAME_BIG}Dto ${CLASS_NAME_SMALL}Dto = ${CLASS_NAME_SMALL}Service.detail(uniId);
        RespDto respDto = new RespDto();
        if (${CLASS_NAME_SMALL}Dto == null) {
            respDto.setSuccess(false);
        }
        respDto.setContent(${CLASS_NAME_SMALL}Dto);
        return respDto;
    }

    @DeleteMapping("/delete/{uniId}")
    public RespDto delete(@PathVariable String uniId, @RequestHeader(value = "domain", required = false) String domain) {
        Boolean flag = ${CLASS_NAME_SMALL}Service.delete(uniId, domain);
        RespDto respDto = new RespDto();
        respDto.setSuccess(flag);
        return respDto;
    }

}
