package com.cevent.template.prod.util;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * @author cevent
 * @create 2025/1/26 23:23
 */
public class FreemarkerSet {

    //主包名：如果当前项目在子目录，需要前缀子目录名称
    public static String mainPkg="cevent_server";
    //子模块名称
    public static String moduleName="template";
    //模版名称
    public static String templateName="template";
    //环境配置
    public static String modeName="prod";
    //ftl名称
    public static String ftlName="dto.ftl";
    //ftl模板路径（test）
    public static String ftlPath=moduleName+ "\\src\\main\\java\\com\\cevent\\"+templateName+"\\"+modeName+"\\ftl\\";

    //文件生成目标:目录
    public static String filePath=moduleName+ "\\src\\main\\java\\com\\cevent\\"+moduleName+"\\"+modeName+"\\";
    //文件生成目标：名称
    public static String fileName="TestDto.java";
    //freemarker模板引擎
    public static Template templateEngine;

    //ftk文件引用的变量
    public static String CLASS_NAME_BIG="TestName";
    public static String CLASS_NAME_SMALL="testName";
    public static String TABLE_NAME_CN="TestTable";

    //1.初始化freemarker
    public static void initConfig(String ftlName) throws IOException {
        Configuration configuration=new Configuration(Configuration.VERSION_2_3_33);
        configuration.setDirectoryForTemplateLoading(new File(ftlPath));
        configuration.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_33));

        templateEngine=configuration.getTemplate(ftlName);
    }

    /**
     * 2.生成文件
     * @param targetFileName 文件名
     * @param map
     */
    public static void generateFile(String targetFileName, Map<String,Object> map) throws IOException, TemplateException {
        //1.文件名
        FileWriter fw=new FileWriter(targetFileName);
        BufferedWriter bw=new BufferedWriter(fw);

        templateEngine.process(map,bw);
        bw.flush();
        fw.close();
    }

    //测试生成DTO
    public static void main (String [] args) throws IOException, TemplateException {
        initConfig(ftlName);
        generateFile(filePath+fileName,null);
    }
}
