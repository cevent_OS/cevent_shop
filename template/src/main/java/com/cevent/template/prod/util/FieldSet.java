package com.cevent.template.prod.util;

/**
 * @author cevent
 * @create 2025/1/26 23:22
 */
public class FieldSet {
    //字段名
    private String name;
    //字段名 转小驼峰
    private String nameSmallHump;
    //字段名 转大驼峰
    private String nameBigHump;
    //字段名 中文名
    private String nameCn;
    //字段类型
    private String type;
    //java类型，如char/varchar类型转为string
    private String typeJava;
    //表注释
    private String comment;

    //是否允许为空
    private Boolean nullEnable;
    //长度
    private Integer length;
    //是否为枚举，是枚举-添加枚举常量
    private Boolean isEnum;
    //枚举常量，ENUMS_CONST
    private String enumConst;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameSmallHump() {
        return nameSmallHump;
    }

    public void setNameSmallHump(String nameSmallHump) {
        this.nameSmallHump = nameSmallHump;
    }

    public String getNameBigHump() {
        return nameBigHump;
    }

    public void setNameBigHump(String nameBigHump) {
        this.nameBigHump = nameBigHump;
    }

    public String getNameCn() {
        return nameCn;
    }

    public void setNameCn(String nameCn) {
        this.nameCn = nameCn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeJava() {
        return typeJava;
    }

    public void setTypeJava(String typeJava) {
        this.typeJava = typeJava;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getNullEnable() {
        return nullEnable;
    }

    public void setNullEnable(Boolean nullEnable) {
        this.nullEnable = nullEnable;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Boolean getEnum() {
        return isEnum;
    }

    public void setEnum(Boolean anEnum) {
        isEnum = anEnum;
    }

    public String getEnumConst() {
        return enumConst;
    }

    public void setEnumConst(String enumConst) {
        this.enumConst = enumConst;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("FieldSet{");
        sb.append("name='").append(name).append('\'');
        sb.append(", nameSmallHump='").append(nameSmallHump).append('\'');
        sb.append(", nameBigHump='").append(nameBigHump).append('\'');
        sb.append(", nameCn='").append(nameCn).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", typeJava='").append(typeJava).append('\'');
        sb.append(", comment='").append(comment).append('\'');
        sb.append(", nullEnable=").append(nullEnable);
        sb.append(", length=").append(length);
        sb.append(", isEnum=").append(isEnum);
        sb.append(", enumConst='").append(enumConst).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
