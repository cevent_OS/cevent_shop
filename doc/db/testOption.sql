
drop table if exists `test`;
CREATE TABLE `test`
(
    `id`   varchar(255) NOT NULL DEFAULT '1' comment 'id',
    `name` varchar(255)          DEFAULT NULL comment '名称',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  comment='测试';

insert into `test` (`id`,`name`) values (1,'cevent');
insert into `test` (`id`,`name`) values (2,'刘一刀');
insert into `test` (`id`,`name`) values (3,'玥玥');