################################1.模块分类表##############################################
DROP TABLE IF EXISTS `module_category`;
CREATE TABLE `module_category`
(
    `uni_id`         char(8)      NOT NULL DEFAULT '' COMMENT '模块ID',
    `sort`           int(11)               DEFAULT NULL COMMENT '排序',
    `name`           varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
    `level`          int(11)               DEFAULT NULL COMMENT '分类层级',
    `is_show`        int(11)               DEFAULT '1' COMMENT '显示1 || 不显示0',
    `parent_id`      char(8)      NOT NULL DEFAULT '' COMMENT '父ID',
    `article_uni_id` char(8)               DEFAULT NULL COMMENT '文章ID',
    `product_uni_id` char(8)               DEFAULT NULL COMMENT '产品ID',
    `domain_id`      char(8)               DEFAULT NULL COMMENT '站点ID',
    PRIMARY KEY (`uni_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='模块分类表';

#更改表
alter table `module_category`
    add column `create_time` datetime(3) default null comment '创建时间' after `domain_id`;
alter table `module_category`
    add column `update_time` datetime(3) default null comment '更新时间' after `create_time`;

#测试数据
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (2, 3, 'cevent1', 4, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (3, 3, 'cevent2', 5, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (4, 3, 'cevent3', 6, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (5, 3, 'cevent4', 7, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (6, 3, 'cevent5', 8, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (7, 3, 'cevent6', 9, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (8, 3, 'cevent7', 10, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (9, 3, 'cevent8', 11, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (10, 3, 'cevent9', 12, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (11, 3, 'cevent10', 13, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (12, 3, 'cevent11', 14, 1);
insert into `module_category` (`uni_id`, `sort`, `name`, `level`, `is_show`)
values (13, 3, 'cevent12', 15, 1);

#测试批量修改parentId和sort
update `module_category` mc
set mc.parent_id='3QBclU7l',mc.sort=11
where mc.uni_id='MKvtzKu6';


################################2.模块-基础模块表（首页、公司介绍、产品中心、新闻中心、视频中心、联系我们）##############################################
drop table if exists `module_base`;
CREATE TABLE `module_base`
(
    `uni_id`            char(8) NOT NULL COMMENT '唯一ID',
    `name`              varchar(255)                    DEFAULT NULL COMMENT '模块名称',
    `seo_title`         varchar(255)                    DEFAULT NULL COMMENT 'SEO标题',
    `seo_keyword`       varchar(255)                    DEFAULT NULL COMMENT 'SEO关键字',
    `seo_description`   varchar(255)                    DEFAULT NULL COMMENT 'SEO描述',
    `content`           mediumtext COMMENT '模块内容|富文本',
    `pic_list`          longtext COMMENT '图片列表',
    `mini_pic_list`     longtext COMMENT '手机端图片列表',
    `icon_list`         longtext COMMENT '核心内容图标',
    `core_desc`         longtext COMMENT '核心模块描述|{title,desc,icon}',
    `parent_id`         char(8) NOT NULL                DEFAULT '0' COMMENT '父ID',
    `pic_cover`         varchar(255)                    DEFAULT NULL COMMENT '模块封面',
    `video_time`        int(11)                         DEFAULT '0' COMMENT '时长|单位秒',
    `category_id`       char(8) NOT NULL                DEFAULT '0' COMMENT '模块分类',
    `com_type`          char(8)                         DEFAULT NULL COMMENT '公司介绍分类',
    `stock_holder_rate` decimal(8, 2) unsigned zerofill DEFAULT NULL COMMENT '股份占比',
    `content_recommend` char(8)                         DEFAULT NULL COMMENT '内容推送|R广告推送D默认模块',
    `video_type`        char(8)                         DEFAULT NULL COMMENT '视频分类',
    `video`             varchar(500)                    DEFAULT NULL COMMENT '模块视频',
    `video_cover`       varchar(500)                    DEFAULT NULL COMMENT '视频封面',
    `video_title`       varchar(255)                    DEFAULT NULL COMMENT '视频标题',
    `video_desc`        varchar(500)                    DEFAULT NULL COMMENT '视频描述',
    `video_charge`      char(1)                         DEFAULT NULL COMMENT '是否收费||C收费|F免费',
    `ppt_cover`         varchar(500)                    DEFAULT NULL COMMENT 'PPT封面',
    `ppt_title`         varchar(255)                    DEFAULT NULL COMMENT 'PPT标题',
    `ppt_desc`          varchar(500)                    DEFAULT NULL COMMENT 'PPT描述',
    `ppt_file`          varchar(500)                    DEFAULT NULL COMMENT 'PPT课件',
    `design_cover`      varchar(500)                    DEFAULT NULL COMMENT '设计原稿样片',
    `design_title`      varchar(255)                    DEFAULT NULL COMMENT '设计原稿名称',
    `design_desc`       varchar(500)                    DEFAULT NULL COMMENT '设计原稿描述',
    `design_file`       varchar(500)                    DEFAULT NULL COMMENT '设计原稿文件|zip压缩包',
    `module_path`       varchar(255)                    DEFAULT NULL COMMENT '模块路径',
    `template_path`     varchar(255)                    DEFAULT NULL COMMENT '引用模板路径',
    `is_show`           int(11)                         DEFAULT '1' COMMENT '显示1 || 不显示0',
    `sort`              int(11)                         DEFAULT '0' COMMENT '排序',
    `create_time`       datetime(3)                     DEFAULT NULL COMMENT '创建时间',
    `update_time`       datetime(3)                     DEFAULT NULL COMMENT '修改时间',
    `domain_id`         char(8)                         DEFAULT NULL COMMENT '站点ID',
    PRIMARY KEY (`uni_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='模块基础表';
# 修改列名
#8.0以上版本
alter table `module_base` rename column `type_id` to `category_id`;
#5.7
alter table `module_base` change column `type_id` `category_id` char(8);

#修改列
alter table `module_base` modify column `category_id` char(8) default '0' comment '模块分类';


################################3.模块-产品分类表##############################################

################################4.模块-产品内容表##############################################

################################5.模块-文章分类表##############################################

################################6.模块-文章内容表##############################################

################################7.模块-视频分类表##############################################

################################8.模块-视频内容表##############################################


################################9.模块-关联表（产品、文章、普通关联）##############################################


################################10.模块-留言表##############################################

################################11.模块-留言关联表（普通、产品、文章、视频）表##############################################

################################12.系统：租户表（站点设置-域名）##############################################

################################13.系统：资源表（权限路由资源）##############################################

################################14.系统：角色表（角色对应资源）##############################################

################################15.系统：员工表（员工账号信息）##############################################

################################16.系统：角色-员工关联表（员工-角色权限）##############################################

################################17.三方资源：短信表 ##############################################

################################18.三方资源：图像文字识别表 ##############################################

################################19.三方资源：语音识别表 ##############################################

################################20.三方资源：友情链接表 ##############################################

################################21.三方资源：微信公众平台配置表 ##############################################

################################22.会员管理：会员分类表（等级、分类信息） ##############################################

################################23.会员管理：会员信息表（基本信息） ##############################################

################################24.营销管理：会员-折扣配置表 ##############################################

################################25.会员管理：会员-折扣关联表 ##############################################

################################26.文件管理：文件搜索及关联对象表（不可删） ##############################################

