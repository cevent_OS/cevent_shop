package com.cevent.eureka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.core.env.Environment;

/**
 * @author cevent
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {
    //日志配置
    private static final Logger LOG = LoggerFactory.getLogger(EurekaApplication.class);

    public static void main(String[] args) {
        SpringApplication app=new SpringApplication(EurekaApplication.class);
        Environment env=app.run(args).getEnvironment();
        System.out.println("启动");
        LOG.info("启动成功");
        LOG.info("Eureka启动地址：\t http://127.0.0.1:{}",env.getProperty("server.port"));
    }

}