package com.cevent.common.exception;

/**
 * @author cevent
 * @create 2025/1/10 2:41
 * 一般业务异常用于RuntimeException(不需要try-catch包裹)
 */
public class ValidatorException extends RuntimeException{
    //super
    public ValidatorException(String msg){
        super(msg);
    }
}
