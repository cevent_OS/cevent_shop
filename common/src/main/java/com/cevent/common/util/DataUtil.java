package com.cevent.common.util;

import com.cevent.common.dto.ModuleCategoryChildrenDto;
import com.cevent.common.dto.NodeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cevent
 * @create 2025/2/2 19:20
 */
public class DataUtil {
    private static final Logger LOG= LoggerFactory.getLogger(DataUtil.class);

    public static List flattenChildren(List<ModuleCategoryChildrenDto> childrenList){
        List<ModuleCategoryChildrenDto> result=new ArrayList<>();
        for(ModuleCategoryChildrenDto node:childrenList){
            result.add(node);
            if(node.getChildren()!=null && !node.getChildren().isEmpty()){
                // 递归调用，并将结果添加到结果列表中
                result.addAll(flattenChildren(node.getChildren()));
                // 清空当前节点的children，因为我们已经在结果列表中处理了它们
                node.setChildren(new ArrayList<>());
            }
        }
        return result;
    }
}
