package com.cevent.common.util;

import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cevent
 * @create 2025/1/8 12:31
 */
public class CopyUtil {
    //单个类
    public static <T> T copy(Object origin,Class<T> target){
        if(origin==null){
            return null;
        }
        T obj = null;

        try {
            obj= target.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }

        BeanUtils.copyProperties(origin,obj);

        return obj;
    }
    //数组类
    public static <T> List<T> copyList(List originList,Class<T> target){
        List<T> list=new ArrayList<>();
        if(!CollectionUtils.isEmpty(originList)){
            for (Object source:originList){
                T obj=copy(source,target);
                list.add(obj);
            }
        }

        return list;
    }
}
