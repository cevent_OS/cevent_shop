package com.cevent.common.util;

import com.cevent.common.exception.ValidatorException;
import org.springframework.util.StringUtils;


/**
 * @author cevent
 * @create 2025/1/10 2:36
 */
public class ValidatorUtil {
    // 1.非空判断，避免big decimal报错，这里更改为object
    public static void isEmpty(Object str, String fieldName) {
        if (StringUtils.isEmpty(str) || str==null) {
            throw new ValidatorException(fieldName + "不能为空");
        }
    }

    // 长度校验
    public static void isLength(String str, String fieldName, int min, int max) {
        int length = 0;
        if (!StringUtils.isEmpty(str)) {
            length = str.length();
        }
        if (length < min || length > max) {
            throw new ValidatorException(fieldName + "长度在 " + min + " ~ " + max + " 位");
        }
    }
}
