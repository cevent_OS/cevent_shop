package com.cevent.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * @author cevent
 * @create 2025/1/8 10:23
 */
public class UuidUtil {

    private static final Logger LOG= LoggerFactory.getLogger(UuidUtil.class);
    //随机字符串：64个数字+字母(26个小写字母*2+（1-10数字）)。
    //根据32位ID，转为62进制的8位ID，减少存储空间。原理是将UUID转为十进制，再对62取余，也可以再增加2个字符，转为64进制
    public static String[] chars=new String[]{
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
            "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    };

    //机器生成的32位ID
    public static String getUuid32(){
        String uuid= UUID.randomUUID().toString();
        String uuidStr=uuid.replaceAll("-","");
        // LOG.info("uuid: {} , uuidString: {}",uuid,uuidStr);
        return uuidStr;
    };

    //封装8为短ID
    public static String getUuid8(){
        StringBuffer sb=new StringBuffer();
        String uuid=UuidUtil.getUuid32();
        for (int i = 0, l = 8; i < l; i++) {
            //从32位字符串截取id
            String id=uuid.substring(i*4,i*4+4);
            //将id转为16进制解析
            int ids=Integer.parseInt(id,16);
            //对62取余
            sb.append(chars[ids%0x3E]);
        }
        return sb.toString();
    };

    public static void main(String[] args) {
        getUuid32();
        LOG.info(getUuid8());
    }
}
