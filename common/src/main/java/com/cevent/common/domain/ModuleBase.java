package com.cevent.common.domain;

import java.math.BigDecimal;
import java.util.Date;

public class ModuleBase {
    private String uniId;

    private String name;

    private String seoTitle;

    private String seoKeyword;

    private String seoDescription;

    private String parentId;

    private String picCover;

    private Integer videoTime;

    private String categoryId;

    private String comType;

    private BigDecimal stockHolderRate;

    private String contentRecommend;

    private String videoType;

    private String video;

    private String videoCover;

    private String videoTitle;

    private String videoDesc;

    private String videoCharge;

    private String pptCover;

    private String pptTitle;

    private String pptDesc;

    private String pptFile;

    private String designCover;

    private String designTitle;

    private String designDesc;

    private String designFile;

    private String modulePath;

    private String templatePath;

    private Integer isShow;

    private Integer sort;

    private Date createTime;

    private Date updateTime;

    private String domainId;

    private String content;

    private String picList;

    private String miniPicList;

    private String iconList;

    private String coreDesc;

    public String getUniId() {
        return uniId;
    }

    public void setUniId(String uniId) {
        this.uniId = uniId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoKeyword() {
        return seoKeyword;
    }

    public void setSeoKeyword(String seoKeyword) {
        this.seoKeyword = seoKeyword;
    }

    public String getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPicCover() {
        return picCover;
    }

    public void setPicCover(String picCover) {
        this.picCover = picCover;
    }

    public Integer getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(Integer videoTime) {
        this.videoTime = videoTime;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getComType() {
        return comType;
    }

    public void setComType(String comType) {
        this.comType = comType;
    }

    public BigDecimal getStockHolderRate() {
        return stockHolderRate;
    }

    public void setStockHolderRate(BigDecimal stockHolderRate) {
        this.stockHolderRate = stockHolderRate;
    }

    public String getContentRecommend() {
        return contentRecommend;
    }

    public void setContentRecommend(String contentRecommend) {
        this.contentRecommend = contentRecommend;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoCover() {
        return videoCover;
    }

    public void setVideoCover(String videoCover) {
        this.videoCover = videoCover;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoDesc() {
        return videoDesc;
    }

    public void setVideoDesc(String videoDesc) {
        this.videoDesc = videoDesc;
    }

    public String getVideoCharge() {
        return videoCharge;
    }

    public void setVideoCharge(String videoCharge) {
        this.videoCharge = videoCharge;
    }

    public String getPptCover() {
        return pptCover;
    }

    public void setPptCover(String pptCover) {
        this.pptCover = pptCover;
    }

    public String getPptTitle() {
        return pptTitle;
    }

    public void setPptTitle(String pptTitle) {
        this.pptTitle = pptTitle;
    }

    public String getPptDesc() {
        return pptDesc;
    }

    public void setPptDesc(String pptDesc) {
        this.pptDesc = pptDesc;
    }

    public String getPptFile() {
        return pptFile;
    }

    public void setPptFile(String pptFile) {
        this.pptFile = pptFile;
    }

    public String getDesignCover() {
        return designCover;
    }

    public void setDesignCover(String designCover) {
        this.designCover = designCover;
    }

    public String getDesignTitle() {
        return designTitle;
    }

    public void setDesignTitle(String designTitle) {
        this.designTitle = designTitle;
    }

    public String getDesignDesc() {
        return designDesc;
    }

    public void setDesignDesc(String designDesc) {
        this.designDesc = designDesc;
    }

    public String getDesignFile() {
        return designFile;
    }

    public void setDesignFile(String designFile) {
        this.designFile = designFile;
    }

    public String getModulePath() {
        return modulePath;
    }

    public void setModulePath(String modulePath) {
        this.modulePath = modulePath;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicList() {
        return picList;
    }

    public void setPicList(String picList) {
        this.picList = picList;
    }

    public String getMiniPicList() {
        return miniPicList;
    }

    public void setMiniPicList(String miniPicList) {
        this.miniPicList = miniPicList;
    }

    public String getIconList() {
        return iconList;
    }

    public void setIconList(String iconList) {
        this.iconList = iconList;
    }

    public String getCoreDesc() {
        return coreDesc;
    }

    public void setCoreDesc(String coreDesc) {
        this.coreDesc = coreDesc;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uniId=").append(uniId);
        sb.append(", name=").append(name);
        sb.append(", seoTitle=").append(seoTitle);
        sb.append(", seoKeyword=").append(seoKeyword);
        sb.append(", seoDescription=").append(seoDescription);
        sb.append(", parentId=").append(parentId);
        sb.append(", picCover=").append(picCover);
        sb.append(", videoTime=").append(videoTime);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", comType=").append(comType);
        sb.append(", stockHolderRate=").append(stockHolderRate);
        sb.append(", contentRecommend=").append(contentRecommend);
        sb.append(", videoType=").append(videoType);
        sb.append(", video=").append(video);
        sb.append(", videoCover=").append(videoCover);
        sb.append(", videoTitle=").append(videoTitle);
        sb.append(", videoDesc=").append(videoDesc);
        sb.append(", videoCharge=").append(videoCharge);
        sb.append(", pptCover=").append(pptCover);
        sb.append(", pptTitle=").append(pptTitle);
        sb.append(", pptDesc=").append(pptDesc);
        sb.append(", pptFile=").append(pptFile);
        sb.append(", designCover=").append(designCover);
        sb.append(", designTitle=").append(designTitle);
        sb.append(", designDesc=").append(designDesc);
        sb.append(", designFile=").append(designFile);
        sb.append(", modulePath=").append(modulePath);
        sb.append(", templatePath=").append(templatePath);
        sb.append(", isShow=").append(isShow);
        sb.append(", sort=").append(sort);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", domainId=").append(domainId);
        sb.append(", content=").append(content);
        sb.append(", picList=").append(picList);
        sb.append(", miniPicList=").append(miniPicList);
        sb.append(", iconList=").append(iconList);
        sb.append(", coreDesc=").append(coreDesc);
        sb.append("]");
        return sb.toString();
    }
}