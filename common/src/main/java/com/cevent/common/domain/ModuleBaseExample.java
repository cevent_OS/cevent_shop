package com.cevent.common.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModuleBaseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ModuleBaseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUniIdIsNull() {
            addCriterion("uni_id is null");
            return (Criteria) this;
        }

        public Criteria andUniIdIsNotNull() {
            addCriterion("uni_id is not null");
            return (Criteria) this;
        }

        public Criteria andUniIdEqualTo(String value) {
            addCriterion("uni_id =", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdNotEqualTo(String value) {
            addCriterion("uni_id <>", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdGreaterThan(String value) {
            addCriterion("uni_id >", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdGreaterThanOrEqualTo(String value) {
            addCriterion("uni_id >=", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdLessThan(String value) {
            addCriterion("uni_id <", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdLessThanOrEqualTo(String value) {
            addCriterion("uni_id <=", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdLike(String value) {
            addCriterion("uni_id like", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdNotLike(String value) {
            addCriterion("uni_id not like", value, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdIn(List<String> values) {
            addCriterion("uni_id in", values, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdNotIn(List<String> values) {
            addCriterion("uni_id not in", values, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdBetween(String value1, String value2) {
            addCriterion("uni_id between", value1, value2, "uniId");
            return (Criteria) this;
        }

        public Criteria andUniIdNotBetween(String value1, String value2) {
            addCriterion("uni_id not between", value1, value2, "uniId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("`name` is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("`name` is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("`name` =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("`name` <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("`name` >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("`name` >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("`name` <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("`name` <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("`name` like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("`name` not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("`name` in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("`name` not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("`name` between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("`name` not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSeoTitleIsNull() {
            addCriterion("seo_title is null");
            return (Criteria) this;
        }

        public Criteria andSeoTitleIsNotNull() {
            addCriterion("seo_title is not null");
            return (Criteria) this;
        }

        public Criteria andSeoTitleEqualTo(String value) {
            addCriterion("seo_title =", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleNotEqualTo(String value) {
            addCriterion("seo_title <>", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleGreaterThan(String value) {
            addCriterion("seo_title >", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleGreaterThanOrEqualTo(String value) {
            addCriterion("seo_title >=", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleLessThan(String value) {
            addCriterion("seo_title <", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleLessThanOrEqualTo(String value) {
            addCriterion("seo_title <=", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleLike(String value) {
            addCriterion("seo_title like", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleNotLike(String value) {
            addCriterion("seo_title not like", value, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleIn(List<String> values) {
            addCriterion("seo_title in", values, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleNotIn(List<String> values) {
            addCriterion("seo_title not in", values, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleBetween(String value1, String value2) {
            addCriterion("seo_title between", value1, value2, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoTitleNotBetween(String value1, String value2) {
            addCriterion("seo_title not between", value1, value2, "seoTitle");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordIsNull() {
            addCriterion("seo_keyword is null");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordIsNotNull() {
            addCriterion("seo_keyword is not null");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordEqualTo(String value) {
            addCriterion("seo_keyword =", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordNotEqualTo(String value) {
            addCriterion("seo_keyword <>", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordGreaterThan(String value) {
            addCriterion("seo_keyword >", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordGreaterThanOrEqualTo(String value) {
            addCriterion("seo_keyword >=", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordLessThan(String value) {
            addCriterion("seo_keyword <", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordLessThanOrEqualTo(String value) {
            addCriterion("seo_keyword <=", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordLike(String value) {
            addCriterion("seo_keyword like", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordNotLike(String value) {
            addCriterion("seo_keyword not like", value, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordIn(List<String> values) {
            addCriterion("seo_keyword in", values, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordNotIn(List<String> values) {
            addCriterion("seo_keyword not in", values, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordBetween(String value1, String value2) {
            addCriterion("seo_keyword between", value1, value2, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoKeywordNotBetween(String value1, String value2) {
            addCriterion("seo_keyword not between", value1, value2, "seoKeyword");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionIsNull() {
            addCriterion("seo_description is null");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionIsNotNull() {
            addCriterion("seo_description is not null");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionEqualTo(String value) {
            addCriterion("seo_description =", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionNotEqualTo(String value) {
            addCriterion("seo_description <>", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionGreaterThan(String value) {
            addCriterion("seo_description >", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("seo_description >=", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionLessThan(String value) {
            addCriterion("seo_description <", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionLessThanOrEqualTo(String value) {
            addCriterion("seo_description <=", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionLike(String value) {
            addCriterion("seo_description like", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionNotLike(String value) {
            addCriterion("seo_description not like", value, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionIn(List<String> values) {
            addCriterion("seo_description in", values, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionNotIn(List<String> values) {
            addCriterion("seo_description not in", values, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionBetween(String value1, String value2) {
            addCriterion("seo_description between", value1, value2, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andSeoDescriptionNotBetween(String value1, String value2) {
            addCriterion("seo_description not between", value1, value2, "seoDescription");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parent_id is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(String value) {
            addCriterion("parent_id =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(String value) {
            addCriterion("parent_id <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(String value) {
            addCriterion("parent_id >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(String value) {
            addCriterion("parent_id >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(String value) {
            addCriterion("parent_id <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(String value) {
            addCriterion("parent_id <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLike(String value) {
            addCriterion("parent_id like", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotLike(String value) {
            addCriterion("parent_id not like", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<String> values) {
            addCriterion("parent_id in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<String> values) {
            addCriterion("parent_id not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(String value1, String value2) {
            addCriterion("parent_id between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(String value1, String value2) {
            addCriterion("parent_id not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andPicCoverIsNull() {
            addCriterion("pic_cover is null");
            return (Criteria) this;
        }

        public Criteria andPicCoverIsNotNull() {
            addCriterion("pic_cover is not null");
            return (Criteria) this;
        }

        public Criteria andPicCoverEqualTo(String value) {
            addCriterion("pic_cover =", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverNotEqualTo(String value) {
            addCriterion("pic_cover <>", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverGreaterThan(String value) {
            addCriterion("pic_cover >", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverGreaterThanOrEqualTo(String value) {
            addCriterion("pic_cover >=", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverLessThan(String value) {
            addCriterion("pic_cover <", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverLessThanOrEqualTo(String value) {
            addCriterion("pic_cover <=", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverLike(String value) {
            addCriterion("pic_cover like", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverNotLike(String value) {
            addCriterion("pic_cover not like", value, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverIn(List<String> values) {
            addCriterion("pic_cover in", values, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverNotIn(List<String> values) {
            addCriterion("pic_cover not in", values, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverBetween(String value1, String value2) {
            addCriterion("pic_cover between", value1, value2, "picCover");
            return (Criteria) this;
        }

        public Criteria andPicCoverNotBetween(String value1, String value2) {
            addCriterion("pic_cover not between", value1, value2, "picCover");
            return (Criteria) this;
        }

        public Criteria andVideoTimeIsNull() {
            addCriterion("video_time is null");
            return (Criteria) this;
        }

        public Criteria andVideoTimeIsNotNull() {
            addCriterion("video_time is not null");
            return (Criteria) this;
        }

        public Criteria andVideoTimeEqualTo(Integer value) {
            addCriterion("video_time =", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeNotEqualTo(Integer value) {
            addCriterion("video_time <>", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeGreaterThan(Integer value) {
            addCriterion("video_time >", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("video_time >=", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeLessThan(Integer value) {
            addCriterion("video_time <", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeLessThanOrEqualTo(Integer value) {
            addCriterion("video_time <=", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeIn(List<Integer> values) {
            addCriterion("video_time in", values, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeNotIn(List<Integer> values) {
            addCriterion("video_time not in", values, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeBetween(Integer value1, Integer value2) {
            addCriterion("video_time between", value1, value2, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("video_time not between", value1, value2, "videoTime");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIsNull() {
            addCriterion("category_id is null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIsNotNull() {
            addCriterion("category_id is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdEqualTo(String value) {
            addCriterion("category_id =", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotEqualTo(String value) {
            addCriterion("category_id <>", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThan(String value) {
            addCriterion("category_id >", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThanOrEqualTo(String value) {
            addCriterion("category_id >=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThan(String value) {
            addCriterion("category_id <", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThanOrEqualTo(String value) {
            addCriterion("category_id <=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLike(String value) {
            addCriterion("category_id like", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotLike(String value) {
            addCriterion("category_id not like", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIn(List<String> values) {
            addCriterion("category_id in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotIn(List<String> values) {
            addCriterion("category_id not in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdBetween(String value1, String value2) {
            addCriterion("category_id between", value1, value2, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotBetween(String value1, String value2) {
            addCriterion("category_id not between", value1, value2, "categoryId");
            return (Criteria) this;
        }

        public Criteria andComTypeIsNull() {
            addCriterion("com_type is null");
            return (Criteria) this;
        }

        public Criteria andComTypeIsNotNull() {
            addCriterion("com_type is not null");
            return (Criteria) this;
        }

        public Criteria andComTypeEqualTo(String value) {
            addCriterion("com_type =", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeNotEqualTo(String value) {
            addCriterion("com_type <>", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeGreaterThan(String value) {
            addCriterion("com_type >", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeGreaterThanOrEqualTo(String value) {
            addCriterion("com_type >=", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeLessThan(String value) {
            addCriterion("com_type <", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeLessThanOrEqualTo(String value) {
            addCriterion("com_type <=", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeLike(String value) {
            addCriterion("com_type like", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeNotLike(String value) {
            addCriterion("com_type not like", value, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeIn(List<String> values) {
            addCriterion("com_type in", values, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeNotIn(List<String> values) {
            addCriterion("com_type not in", values, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeBetween(String value1, String value2) {
            addCriterion("com_type between", value1, value2, "comType");
            return (Criteria) this;
        }

        public Criteria andComTypeNotBetween(String value1, String value2) {
            addCriterion("com_type not between", value1, value2, "comType");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateIsNull() {
            addCriterion("stock_holder_rate is null");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateIsNotNull() {
            addCriterion("stock_holder_rate is not null");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateEqualTo(BigDecimal value) {
            addCriterion("stock_holder_rate =", value, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateNotEqualTo(BigDecimal value) {
            addCriterion("stock_holder_rate <>", value, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateGreaterThan(BigDecimal value) {
            addCriterion("stock_holder_rate >", value, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("stock_holder_rate >=", value, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateLessThan(BigDecimal value) {
            addCriterion("stock_holder_rate <", value, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("stock_holder_rate <=", value, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateIn(List<BigDecimal> values) {
            addCriterion("stock_holder_rate in", values, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateNotIn(List<BigDecimal> values) {
            addCriterion("stock_holder_rate not in", values, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("stock_holder_rate between", value1, value2, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andStockHolderRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("stock_holder_rate not between", value1, value2, "stockHolderRate");
            return (Criteria) this;
        }

        public Criteria andContentRecommendIsNull() {
            addCriterion("content_recommend is null");
            return (Criteria) this;
        }

        public Criteria andContentRecommendIsNotNull() {
            addCriterion("content_recommend is not null");
            return (Criteria) this;
        }

        public Criteria andContentRecommendEqualTo(String value) {
            addCriterion("content_recommend =", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendNotEqualTo(String value) {
            addCriterion("content_recommend <>", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendGreaterThan(String value) {
            addCriterion("content_recommend >", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendGreaterThanOrEqualTo(String value) {
            addCriterion("content_recommend >=", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendLessThan(String value) {
            addCriterion("content_recommend <", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendLessThanOrEqualTo(String value) {
            addCriterion("content_recommend <=", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendLike(String value) {
            addCriterion("content_recommend like", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendNotLike(String value) {
            addCriterion("content_recommend not like", value, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendIn(List<String> values) {
            addCriterion("content_recommend in", values, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendNotIn(List<String> values) {
            addCriterion("content_recommend not in", values, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendBetween(String value1, String value2) {
            addCriterion("content_recommend between", value1, value2, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andContentRecommendNotBetween(String value1, String value2) {
            addCriterion("content_recommend not between", value1, value2, "contentRecommend");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIsNull() {
            addCriterion("video_type is null");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIsNotNull() {
            addCriterion("video_type is not null");
            return (Criteria) this;
        }

        public Criteria andVideoTypeEqualTo(String value) {
            addCriterion("video_type =", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotEqualTo(String value) {
            addCriterion("video_type <>", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeGreaterThan(String value) {
            addCriterion("video_type >", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("video_type >=", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLessThan(String value) {
            addCriterion("video_type <", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLessThanOrEqualTo(String value) {
            addCriterion("video_type <=", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeLike(String value) {
            addCriterion("video_type like", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotLike(String value) {
            addCriterion("video_type not like", value, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeIn(List<String> values) {
            addCriterion("video_type in", values, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotIn(List<String> values) {
            addCriterion("video_type not in", values, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeBetween(String value1, String value2) {
            addCriterion("video_type between", value1, value2, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoTypeNotBetween(String value1, String value2) {
            addCriterion("video_type not between", value1, value2, "videoType");
            return (Criteria) this;
        }

        public Criteria andVideoIsNull() {
            addCriterion("video is null");
            return (Criteria) this;
        }

        public Criteria andVideoIsNotNull() {
            addCriterion("video is not null");
            return (Criteria) this;
        }

        public Criteria andVideoEqualTo(String value) {
            addCriterion("video =", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotEqualTo(String value) {
            addCriterion("video <>", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoGreaterThan(String value) {
            addCriterion("video >", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoGreaterThanOrEqualTo(String value) {
            addCriterion("video >=", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoLessThan(String value) {
            addCriterion("video <", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoLessThanOrEqualTo(String value) {
            addCriterion("video <=", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoLike(String value) {
            addCriterion("video like", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotLike(String value) {
            addCriterion("video not like", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoIn(List<String> values) {
            addCriterion("video in", values, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotIn(List<String> values) {
            addCriterion("video not in", values, "video");
            return (Criteria) this;
        }

        public Criteria andVideoBetween(String value1, String value2) {
            addCriterion("video between", value1, value2, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotBetween(String value1, String value2) {
            addCriterion("video not between", value1, value2, "video");
            return (Criteria) this;
        }

        public Criteria andVideoCoverIsNull() {
            addCriterion("video_cover is null");
            return (Criteria) this;
        }

        public Criteria andVideoCoverIsNotNull() {
            addCriterion("video_cover is not null");
            return (Criteria) this;
        }

        public Criteria andVideoCoverEqualTo(String value) {
            addCriterion("video_cover =", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverNotEqualTo(String value) {
            addCriterion("video_cover <>", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverGreaterThan(String value) {
            addCriterion("video_cover >", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverGreaterThanOrEqualTo(String value) {
            addCriterion("video_cover >=", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverLessThan(String value) {
            addCriterion("video_cover <", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverLessThanOrEqualTo(String value) {
            addCriterion("video_cover <=", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverLike(String value) {
            addCriterion("video_cover like", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverNotLike(String value) {
            addCriterion("video_cover not like", value, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverIn(List<String> values) {
            addCriterion("video_cover in", values, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverNotIn(List<String> values) {
            addCriterion("video_cover not in", values, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverBetween(String value1, String value2) {
            addCriterion("video_cover between", value1, value2, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoCoverNotBetween(String value1, String value2) {
            addCriterion("video_cover not between", value1, value2, "videoCover");
            return (Criteria) this;
        }

        public Criteria andVideoTitleIsNull() {
            addCriterion("video_title is null");
            return (Criteria) this;
        }

        public Criteria andVideoTitleIsNotNull() {
            addCriterion("video_title is not null");
            return (Criteria) this;
        }

        public Criteria andVideoTitleEqualTo(String value) {
            addCriterion("video_title =", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleNotEqualTo(String value) {
            addCriterion("video_title <>", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleGreaterThan(String value) {
            addCriterion("video_title >", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleGreaterThanOrEqualTo(String value) {
            addCriterion("video_title >=", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleLessThan(String value) {
            addCriterion("video_title <", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleLessThanOrEqualTo(String value) {
            addCriterion("video_title <=", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleLike(String value) {
            addCriterion("video_title like", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleNotLike(String value) {
            addCriterion("video_title not like", value, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleIn(List<String> values) {
            addCriterion("video_title in", values, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleNotIn(List<String> values) {
            addCriterion("video_title not in", values, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleBetween(String value1, String value2) {
            addCriterion("video_title between", value1, value2, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoTitleNotBetween(String value1, String value2) {
            addCriterion("video_title not between", value1, value2, "videoTitle");
            return (Criteria) this;
        }

        public Criteria andVideoDescIsNull() {
            addCriterion("video_desc is null");
            return (Criteria) this;
        }

        public Criteria andVideoDescIsNotNull() {
            addCriterion("video_desc is not null");
            return (Criteria) this;
        }

        public Criteria andVideoDescEqualTo(String value) {
            addCriterion("video_desc =", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescNotEqualTo(String value) {
            addCriterion("video_desc <>", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescGreaterThan(String value) {
            addCriterion("video_desc >", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescGreaterThanOrEqualTo(String value) {
            addCriterion("video_desc >=", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescLessThan(String value) {
            addCriterion("video_desc <", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescLessThanOrEqualTo(String value) {
            addCriterion("video_desc <=", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescLike(String value) {
            addCriterion("video_desc like", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescNotLike(String value) {
            addCriterion("video_desc not like", value, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescIn(List<String> values) {
            addCriterion("video_desc in", values, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescNotIn(List<String> values) {
            addCriterion("video_desc not in", values, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescBetween(String value1, String value2) {
            addCriterion("video_desc between", value1, value2, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoDescNotBetween(String value1, String value2) {
            addCriterion("video_desc not between", value1, value2, "videoDesc");
            return (Criteria) this;
        }

        public Criteria andVideoChargeIsNull() {
            addCriterion("video_charge is null");
            return (Criteria) this;
        }

        public Criteria andVideoChargeIsNotNull() {
            addCriterion("video_charge is not null");
            return (Criteria) this;
        }

        public Criteria andVideoChargeEqualTo(String value) {
            addCriterion("video_charge =", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeNotEqualTo(String value) {
            addCriterion("video_charge <>", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeGreaterThan(String value) {
            addCriterion("video_charge >", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeGreaterThanOrEqualTo(String value) {
            addCriterion("video_charge >=", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeLessThan(String value) {
            addCriterion("video_charge <", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeLessThanOrEqualTo(String value) {
            addCriterion("video_charge <=", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeLike(String value) {
            addCriterion("video_charge like", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeNotLike(String value) {
            addCriterion("video_charge not like", value, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeIn(List<String> values) {
            addCriterion("video_charge in", values, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeNotIn(List<String> values) {
            addCriterion("video_charge not in", values, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeBetween(String value1, String value2) {
            addCriterion("video_charge between", value1, value2, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andVideoChargeNotBetween(String value1, String value2) {
            addCriterion("video_charge not between", value1, value2, "videoCharge");
            return (Criteria) this;
        }

        public Criteria andPptCoverIsNull() {
            addCriterion("ppt_cover is null");
            return (Criteria) this;
        }

        public Criteria andPptCoverIsNotNull() {
            addCriterion("ppt_cover is not null");
            return (Criteria) this;
        }

        public Criteria andPptCoverEqualTo(String value) {
            addCriterion("ppt_cover =", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverNotEqualTo(String value) {
            addCriterion("ppt_cover <>", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverGreaterThan(String value) {
            addCriterion("ppt_cover >", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverGreaterThanOrEqualTo(String value) {
            addCriterion("ppt_cover >=", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverLessThan(String value) {
            addCriterion("ppt_cover <", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverLessThanOrEqualTo(String value) {
            addCriterion("ppt_cover <=", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverLike(String value) {
            addCriterion("ppt_cover like", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverNotLike(String value) {
            addCriterion("ppt_cover not like", value, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverIn(List<String> values) {
            addCriterion("ppt_cover in", values, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverNotIn(List<String> values) {
            addCriterion("ppt_cover not in", values, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverBetween(String value1, String value2) {
            addCriterion("ppt_cover between", value1, value2, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptCoverNotBetween(String value1, String value2) {
            addCriterion("ppt_cover not between", value1, value2, "pptCover");
            return (Criteria) this;
        }

        public Criteria andPptTitleIsNull() {
            addCriterion("ppt_title is null");
            return (Criteria) this;
        }

        public Criteria andPptTitleIsNotNull() {
            addCriterion("ppt_title is not null");
            return (Criteria) this;
        }

        public Criteria andPptTitleEqualTo(String value) {
            addCriterion("ppt_title =", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleNotEqualTo(String value) {
            addCriterion("ppt_title <>", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleGreaterThan(String value) {
            addCriterion("ppt_title >", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleGreaterThanOrEqualTo(String value) {
            addCriterion("ppt_title >=", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleLessThan(String value) {
            addCriterion("ppt_title <", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleLessThanOrEqualTo(String value) {
            addCriterion("ppt_title <=", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleLike(String value) {
            addCriterion("ppt_title like", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleNotLike(String value) {
            addCriterion("ppt_title not like", value, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleIn(List<String> values) {
            addCriterion("ppt_title in", values, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleNotIn(List<String> values) {
            addCriterion("ppt_title not in", values, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleBetween(String value1, String value2) {
            addCriterion("ppt_title between", value1, value2, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptTitleNotBetween(String value1, String value2) {
            addCriterion("ppt_title not between", value1, value2, "pptTitle");
            return (Criteria) this;
        }

        public Criteria andPptDescIsNull() {
            addCriterion("ppt_desc is null");
            return (Criteria) this;
        }

        public Criteria andPptDescIsNotNull() {
            addCriterion("ppt_desc is not null");
            return (Criteria) this;
        }

        public Criteria andPptDescEqualTo(String value) {
            addCriterion("ppt_desc =", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescNotEqualTo(String value) {
            addCriterion("ppt_desc <>", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescGreaterThan(String value) {
            addCriterion("ppt_desc >", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescGreaterThanOrEqualTo(String value) {
            addCriterion("ppt_desc >=", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescLessThan(String value) {
            addCriterion("ppt_desc <", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescLessThanOrEqualTo(String value) {
            addCriterion("ppt_desc <=", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescLike(String value) {
            addCriterion("ppt_desc like", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescNotLike(String value) {
            addCriterion("ppt_desc not like", value, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescIn(List<String> values) {
            addCriterion("ppt_desc in", values, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescNotIn(List<String> values) {
            addCriterion("ppt_desc not in", values, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescBetween(String value1, String value2) {
            addCriterion("ppt_desc between", value1, value2, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptDescNotBetween(String value1, String value2) {
            addCriterion("ppt_desc not between", value1, value2, "pptDesc");
            return (Criteria) this;
        }

        public Criteria andPptFileIsNull() {
            addCriterion("ppt_file is null");
            return (Criteria) this;
        }

        public Criteria andPptFileIsNotNull() {
            addCriterion("ppt_file is not null");
            return (Criteria) this;
        }

        public Criteria andPptFileEqualTo(String value) {
            addCriterion("ppt_file =", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileNotEqualTo(String value) {
            addCriterion("ppt_file <>", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileGreaterThan(String value) {
            addCriterion("ppt_file >", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileGreaterThanOrEqualTo(String value) {
            addCriterion("ppt_file >=", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileLessThan(String value) {
            addCriterion("ppt_file <", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileLessThanOrEqualTo(String value) {
            addCriterion("ppt_file <=", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileLike(String value) {
            addCriterion("ppt_file like", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileNotLike(String value) {
            addCriterion("ppt_file not like", value, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileIn(List<String> values) {
            addCriterion("ppt_file in", values, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileNotIn(List<String> values) {
            addCriterion("ppt_file not in", values, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileBetween(String value1, String value2) {
            addCriterion("ppt_file between", value1, value2, "pptFile");
            return (Criteria) this;
        }

        public Criteria andPptFileNotBetween(String value1, String value2) {
            addCriterion("ppt_file not between", value1, value2, "pptFile");
            return (Criteria) this;
        }

        public Criteria andDesignCoverIsNull() {
            addCriterion("design_cover is null");
            return (Criteria) this;
        }

        public Criteria andDesignCoverIsNotNull() {
            addCriterion("design_cover is not null");
            return (Criteria) this;
        }

        public Criteria andDesignCoverEqualTo(String value) {
            addCriterion("design_cover =", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverNotEqualTo(String value) {
            addCriterion("design_cover <>", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverGreaterThan(String value) {
            addCriterion("design_cover >", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverGreaterThanOrEqualTo(String value) {
            addCriterion("design_cover >=", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverLessThan(String value) {
            addCriterion("design_cover <", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverLessThanOrEqualTo(String value) {
            addCriterion("design_cover <=", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverLike(String value) {
            addCriterion("design_cover like", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverNotLike(String value) {
            addCriterion("design_cover not like", value, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverIn(List<String> values) {
            addCriterion("design_cover in", values, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverNotIn(List<String> values) {
            addCriterion("design_cover not in", values, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverBetween(String value1, String value2) {
            addCriterion("design_cover between", value1, value2, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignCoverNotBetween(String value1, String value2) {
            addCriterion("design_cover not between", value1, value2, "designCover");
            return (Criteria) this;
        }

        public Criteria andDesignTitleIsNull() {
            addCriterion("design_title is null");
            return (Criteria) this;
        }

        public Criteria andDesignTitleIsNotNull() {
            addCriterion("design_title is not null");
            return (Criteria) this;
        }

        public Criteria andDesignTitleEqualTo(String value) {
            addCriterion("design_title =", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleNotEqualTo(String value) {
            addCriterion("design_title <>", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleGreaterThan(String value) {
            addCriterion("design_title >", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleGreaterThanOrEqualTo(String value) {
            addCriterion("design_title >=", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleLessThan(String value) {
            addCriterion("design_title <", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleLessThanOrEqualTo(String value) {
            addCriterion("design_title <=", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleLike(String value) {
            addCriterion("design_title like", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleNotLike(String value) {
            addCriterion("design_title not like", value, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleIn(List<String> values) {
            addCriterion("design_title in", values, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleNotIn(List<String> values) {
            addCriterion("design_title not in", values, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleBetween(String value1, String value2) {
            addCriterion("design_title between", value1, value2, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignTitleNotBetween(String value1, String value2) {
            addCriterion("design_title not between", value1, value2, "designTitle");
            return (Criteria) this;
        }

        public Criteria andDesignDescIsNull() {
            addCriterion("design_desc is null");
            return (Criteria) this;
        }

        public Criteria andDesignDescIsNotNull() {
            addCriterion("design_desc is not null");
            return (Criteria) this;
        }

        public Criteria andDesignDescEqualTo(String value) {
            addCriterion("design_desc =", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescNotEqualTo(String value) {
            addCriterion("design_desc <>", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescGreaterThan(String value) {
            addCriterion("design_desc >", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescGreaterThanOrEqualTo(String value) {
            addCriterion("design_desc >=", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescLessThan(String value) {
            addCriterion("design_desc <", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescLessThanOrEqualTo(String value) {
            addCriterion("design_desc <=", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescLike(String value) {
            addCriterion("design_desc like", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescNotLike(String value) {
            addCriterion("design_desc not like", value, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescIn(List<String> values) {
            addCriterion("design_desc in", values, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescNotIn(List<String> values) {
            addCriterion("design_desc not in", values, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescBetween(String value1, String value2) {
            addCriterion("design_desc between", value1, value2, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignDescNotBetween(String value1, String value2) {
            addCriterion("design_desc not between", value1, value2, "designDesc");
            return (Criteria) this;
        }

        public Criteria andDesignFileIsNull() {
            addCriterion("design_file is null");
            return (Criteria) this;
        }

        public Criteria andDesignFileIsNotNull() {
            addCriterion("design_file is not null");
            return (Criteria) this;
        }

        public Criteria andDesignFileEqualTo(String value) {
            addCriterion("design_file =", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileNotEqualTo(String value) {
            addCriterion("design_file <>", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileGreaterThan(String value) {
            addCriterion("design_file >", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileGreaterThanOrEqualTo(String value) {
            addCriterion("design_file >=", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileLessThan(String value) {
            addCriterion("design_file <", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileLessThanOrEqualTo(String value) {
            addCriterion("design_file <=", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileLike(String value) {
            addCriterion("design_file like", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileNotLike(String value) {
            addCriterion("design_file not like", value, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileIn(List<String> values) {
            addCriterion("design_file in", values, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileNotIn(List<String> values) {
            addCriterion("design_file not in", values, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileBetween(String value1, String value2) {
            addCriterion("design_file between", value1, value2, "designFile");
            return (Criteria) this;
        }

        public Criteria andDesignFileNotBetween(String value1, String value2) {
            addCriterion("design_file not between", value1, value2, "designFile");
            return (Criteria) this;
        }

        public Criteria andModulePathIsNull() {
            addCriterion("module_path is null");
            return (Criteria) this;
        }

        public Criteria andModulePathIsNotNull() {
            addCriterion("module_path is not null");
            return (Criteria) this;
        }

        public Criteria andModulePathEqualTo(String value) {
            addCriterion("module_path =", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathNotEqualTo(String value) {
            addCriterion("module_path <>", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathGreaterThan(String value) {
            addCriterion("module_path >", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathGreaterThanOrEqualTo(String value) {
            addCriterion("module_path >=", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathLessThan(String value) {
            addCriterion("module_path <", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathLessThanOrEqualTo(String value) {
            addCriterion("module_path <=", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathLike(String value) {
            addCriterion("module_path like", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathNotLike(String value) {
            addCriterion("module_path not like", value, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathIn(List<String> values) {
            addCriterion("module_path in", values, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathNotIn(List<String> values) {
            addCriterion("module_path not in", values, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathBetween(String value1, String value2) {
            addCriterion("module_path between", value1, value2, "modulePath");
            return (Criteria) this;
        }

        public Criteria andModulePathNotBetween(String value1, String value2) {
            addCriterion("module_path not between", value1, value2, "modulePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathIsNull() {
            addCriterion("template_path is null");
            return (Criteria) this;
        }

        public Criteria andTemplatePathIsNotNull() {
            addCriterion("template_path is not null");
            return (Criteria) this;
        }

        public Criteria andTemplatePathEqualTo(String value) {
            addCriterion("template_path =", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathNotEqualTo(String value) {
            addCriterion("template_path <>", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathGreaterThan(String value) {
            addCriterion("template_path >", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathGreaterThanOrEqualTo(String value) {
            addCriterion("template_path >=", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathLessThan(String value) {
            addCriterion("template_path <", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathLessThanOrEqualTo(String value) {
            addCriterion("template_path <=", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathLike(String value) {
            addCriterion("template_path like", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathNotLike(String value) {
            addCriterion("template_path not like", value, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathIn(List<String> values) {
            addCriterion("template_path in", values, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathNotIn(List<String> values) {
            addCriterion("template_path not in", values, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathBetween(String value1, String value2) {
            addCriterion("template_path between", value1, value2, "templatePath");
            return (Criteria) this;
        }

        public Criteria andTemplatePathNotBetween(String value1, String value2) {
            addCriterion("template_path not between", value1, value2, "templatePath");
            return (Criteria) this;
        }

        public Criteria andIsShowIsNull() {
            addCriterion("is_show is null");
            return (Criteria) this;
        }

        public Criteria andIsShowIsNotNull() {
            addCriterion("is_show is not null");
            return (Criteria) this;
        }

        public Criteria andIsShowEqualTo(Integer value) {
            addCriterion("is_show =", value, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowNotEqualTo(Integer value) {
            addCriterion("is_show <>", value, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowGreaterThan(Integer value) {
            addCriterion("is_show >", value, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_show >=", value, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowLessThan(Integer value) {
            addCriterion("is_show <", value, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowLessThanOrEqualTo(Integer value) {
            addCriterion("is_show <=", value, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowIn(List<Integer> values) {
            addCriterion("is_show in", values, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowNotIn(List<Integer> values) {
            addCriterion("is_show not in", values, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowBetween(Integer value1, Integer value2) {
            addCriterion("is_show between", value1, value2, "isShow");
            return (Criteria) this;
        }

        public Criteria andIsShowNotBetween(Integer value1, Integer value2) {
            addCriterion("is_show not between", value1, value2, "isShow");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andDomainIdIsNull() {
            addCriterion("domain_id is null");
            return (Criteria) this;
        }

        public Criteria andDomainIdIsNotNull() {
            addCriterion("domain_id is not null");
            return (Criteria) this;
        }

        public Criteria andDomainIdEqualTo(String value) {
            addCriterion("domain_id =", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdNotEqualTo(String value) {
            addCriterion("domain_id <>", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdGreaterThan(String value) {
            addCriterion("domain_id >", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdGreaterThanOrEqualTo(String value) {
            addCriterion("domain_id >=", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdLessThan(String value) {
            addCriterion("domain_id <", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdLessThanOrEqualTo(String value) {
            addCriterion("domain_id <=", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdLike(String value) {
            addCriterion("domain_id like", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdNotLike(String value) {
            addCriterion("domain_id not like", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdIn(List<String> values) {
            addCriterion("domain_id in", values, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdNotIn(List<String> values) {
            addCriterion("domain_id not in", values, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdBetween(String value1, String value2) {
            addCriterion("domain_id between", value1, value2, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdNotBetween(String value1, String value2) {
            addCriterion("domain_id not between", value1, value2, "domainId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}