package com.cevent.common.service;

import com.cevent.common.domain.ModuleCategory;
import com.cevent.common.domain.ModuleCategoryExample;
import com.cevent.common.dto.ModuleCategoryChildrenDto;
import com.cevent.common.dto.ModuleCategoryDto;
import com.cevent.common.dto.NodeDto;
import com.cevent.common.dto.PageDto;
import com.cevent.common.mapper.ModuleCategoryMapper;
import com.cevent.common.mapper.defMapper.ModuleCategoryDefMapper;
import com.cevent.common.util.CopyUtil;
import com.cevent.common.util.DataUtil;
import com.cevent.common.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author cevent
 * @create 2024/12/25 14:34
 */
@Service
public class ModuleCategoryService {

    private static final Logger LOG= LoggerFactory.getLogger(ModuleCategoryService.class);

    @Resource
    private ModuleCategoryMapper moduleCategoryMapper;
    @Resource
    private ModuleCategoryDefMapper moduleCategoryDefMapper;

    //如果需要返回指定类型，则指定List<Dto>，这里因为前端传入的pageDto对象，不做处理也会返回PageDto，所以这里没有return
    public void getList(PageDto pageDto,String domain){

        String domainId="";
        if (!StringUtils.isEmpty(domain)){
            domainId="TODO 请求site配置数据，获取domainId";
        }

        PageHelper.startPage(pageDto.getPage(),pageDto.getSize());
        ModuleCategoryExample moduleCategoryExample=new ModuleCategoryExample();
        // moduleCategoryExample.createCriteria().andUniIdEqualTo("1");
        ModuleCategoryExample.Criteria criteria=moduleCategoryExample.createCriteria();
        if(domainId!=""){
            criteria.andDomainIdEqualTo(domainId);
        }
        moduleCategoryExample.setOrderByClause("sort desc");
        List<ModuleCategory> list=moduleCategoryMapper.selectByExample(null);

        //select获取数据后，Pageinfo接收查询结果
        PageInfo<ModuleCategory> pageInfo=new PageInfo<>(list);

        // List 需要 new ArrayList< entity >
        List<ModuleCategoryDto> lists= new ArrayList<ModuleCategoryDto>();
        for (int i = 0, l = list.size(); i < l; i++) {
            ModuleCategory moduleCategory=list.get(i);
            ModuleCategoryDto moduleCategoryDto=new ModuleCategoryDto();
            //赋值
            BeanUtils.copyProperties(moduleCategory,moduleCategoryDto);
            lists.add(moduleCategoryDto);
        }

        //赋值pageinfo List
        pageDto.setTotal(pageInfo.getTotal());
        pageDto.setList(lists);

        //如果不返回分页数据，则输出lists即可，注意list<dto对象>
        // return pageDto;
    }

    //获取tree结构数据
    public List<ModuleCategoryChildrenDto> getTree(String domain){
        String dominId="";
        if(!StringUtils.isEmpty(domain)){
            LOG.info("TODO 请求site配置数据，获取domainId: {}",domain);
        }

        //获取分类
        ModuleCategoryExample example=new ModuleCategoryExample();
        ModuleCategoryExample.Criteria criteria=example.createCriteria();
        // criteria.andDomainIdEqualTo(dominId);
        example.setOrderByClause("sort asc");
        List<ModuleCategory> moduleCategories=moduleCategoryMapper.selectByExample(example);

        //生成children
        List<ModuleCategoryChildrenDto> childrenDtos=CopyUtil.copyList(moduleCategories, ModuleCategoryChildrenDto.class);
        //获取一级分类
        List<ModuleCategoryChildrenDto> level1=childrenDtos.stream().filter((category)->{
            return category.getParentId().equals("0");
        }).map((child)->{
            child.setChildren(getChildren(child,childrenDtos));
            return child;
        }).sorted((cat1,cat2)->{
            return (cat1.getSort() == null ? 0 : cat1.getSort()) - (cat2.getSort() == null ? 0 : cat2.getSort());
        }).collect(Collectors.toList());

        return level1;
    }

    //递归查询子分类
    private List<ModuleCategoryChildrenDto> getChildren(ModuleCategoryChildrenDto childrenDto,List<ModuleCategoryChildrenDto> childrenDtoList){
        List<ModuleCategoryChildrenDto> childrens=childrenDtoList.stream().filter(category->{
            return category.getParentId().equals(childrenDto.getUniId());
        }).map((child)->{
            child.setChildren(getChildren(child,childrenDtoList));
            return child;
        }).sorted((cat1,cat2)->{
            //避免空指针异常
            return (cat1.getSort() == null ? 0 : cat1.getSort()) - (cat2.getSort() == null ? 0 : cat2.getSort());
        }).collect(Collectors.toList());

        return childrens;
    }

    public void save(ModuleCategoryDto moduleCategoryDto,String domain){
        //isEmpty被弃用，hasText可校验""/" "/null
        if(StringUtils.hasText(moduleCategoryDto.getParentId()) || moduleCategoryDto.getParentId()==null){
            moduleCategoryDto.setParentId("0");
        }
        if(!StringUtils.hasText(moduleCategoryDto.getUniId())){
            this.insert(CopyUtil.copy(moduleCategoryDto,ModuleCategory.class));
        }else{
            //优化，只更新传入的字段
            this.update(CopyUtil.copy(moduleCategoryDto,ModuleCategory.class));
        }
    }

    private void insert(ModuleCategory moduleCategory){
        moduleCategory.setUniId(UuidUtil.getUuid8());
        moduleCategoryMapper.insert(moduleCategory);
    }

    private void update(ModuleCategory moduleCategory){

        moduleCategoryMapper.updateByPrimaryKey(moduleCategory);
    }

    public ModuleCategoryDto detail(String uniId){
        ModuleCategory moduleCategory=new ModuleCategory();
        if(uniId != ""){
            moduleCategory=moduleCategoryMapper.selectByPrimaryKey(uniId);
        }
        return CopyUtil.copy(moduleCategory,ModuleCategoryDto.class);
    }

    public Boolean delete(String uniId,String domian){
        //TODO 先根据id和domain查询是否是当前站点的分类数据
        Integer flag=0;
        if(StringUtils.hasText(uniId)){
            flag=moduleCategoryMapper.deleteByPrimaryKey(uniId);
        }

        return flag>0;
    }

    public List<ModuleCategoryChildrenDto> saveTree(List<ModuleCategoryChildrenDto> childrenDtos,String domain){
        String dominId="";
        if(!StringUtils.isEmpty(domain)){
            LOG.info("TODO 请求site配置数据，获取domainId: {}",domain);
        }
        List<ModuleCategoryChildrenDto> result= DataUtil.flattenChildren(childrenDtos);
        List<ModuleCategory> moduleCategoryList=CopyUtil.copyList(result,ModuleCategory.class);
        for (int i = 0, l = moduleCategoryList.size(); i < l; i++) {

            ModuleCategory moduleCategory=moduleCategoryList.get(i);
            // moduleCategoryMapper.updateByPrimaryKeySelective(moduleCategory);
            //自定义mapper
            int flag= moduleCategoryDefMapper.moduleCategoryTreeDragSave(moduleCategory.getUniId(),moduleCategory.getParentId(),moduleCategory.getSort(),moduleCategory.getLevel());

        }
        return result;
    }

}
