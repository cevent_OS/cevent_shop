package com.cevent.common.service;

import com.cevent.common.domain.Test;
import com.cevent.common.domain.TestExample;
import com.cevent.common.mapper.TestMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author cevent
 * @create 2024/12/5 14:02
 */
@Service
public class TestService {
    @Resource
    private TestMapper testMapper;

    public List<Test> getList(){
        return testMapper.selectByExample(null);
    }

    public List<Test> getListByDesc(){
        TestExample testExample=new TestExample();
        testExample.setOrderByClause("id desc");
        return  testMapper.selectByExample(testExample);
    }

    public List<Test> getById(String id){
        TestExample testExample=new TestExample();
        testExample.createCriteria().andIdEqualTo(id);
        return testMapper.selectByExample(testExample);
    }
}
