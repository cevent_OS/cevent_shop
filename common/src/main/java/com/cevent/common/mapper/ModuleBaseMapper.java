package com.cevent.common.mapper;

import com.cevent.common.domain.ModuleBase;
import com.cevent.common.domain.ModuleBaseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ModuleBaseMapper {
    long countByExample(ModuleBaseExample example);

    int deleteByExample(ModuleBaseExample example);

    int deleteByPrimaryKey(String uniId);

    int insert(ModuleBase record);

    int insertSelective(ModuleBase record);

    List<ModuleBase> selectByExampleWithBLOBs(ModuleBaseExample example);

    List<ModuleBase> selectByExample(ModuleBaseExample example);

    ModuleBase selectByPrimaryKey(String uniId);

    int updateByExampleSelective(@Param("record") ModuleBase record, @Param("example") ModuleBaseExample example);

    int updateByExampleWithBLOBs(@Param("record") ModuleBase record, @Param("example") ModuleBaseExample example);

    int updateByExample(@Param("record") ModuleBase record, @Param("example") ModuleBaseExample example);

    int updateByPrimaryKeySelective(ModuleBase record);

    int updateByPrimaryKeyWithBLOBs(ModuleBase record);

    int updateByPrimaryKey(ModuleBase record);
}