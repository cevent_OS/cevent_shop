package com.cevent.common.mapper.defMapper;

import com.cevent.common.dto.ModuleCategoryDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author cevent
 * @create 2025/2/3 22:20
 */
public interface ModuleCategoryDefMapper {
    int moduleCategoryTreeDragSave(@Param("uniId") String uniId,@Param("parentId") String parentId,@Param("sort") int sort,@Param("level") int level);
}
