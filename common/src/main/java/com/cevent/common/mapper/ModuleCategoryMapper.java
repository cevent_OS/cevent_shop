package com.cevent.common.mapper;

import com.cevent.common.domain.ModuleCategory;
import com.cevent.common.domain.ModuleCategoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ModuleCategoryMapper {
    long countByExample(ModuleCategoryExample example);

    int deleteByExample(ModuleCategoryExample example);

    int deleteByPrimaryKey(String uniId);

    int insert(ModuleCategory record);

    int insertSelective(ModuleCategory record);

    List<ModuleCategory> selectByExample(ModuleCategoryExample example);

    ModuleCategory selectByPrimaryKey(String uniId);

    int updateByExampleSelective(@Param("record") ModuleCategory record, @Param("example") ModuleCategoryExample example);

    int updateByExample(@Param("record") ModuleCategory record, @Param("example") ModuleCategoryExample example);

    int updateByPrimaryKeySelective(ModuleCategory record);

    int updateByPrimaryKey(ModuleCategory record);
}