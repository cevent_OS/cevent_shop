package com.cevent.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author cevent
 * @create 2024/12/25 22:25
 */
// @Configuration
// public class CorsConfig implements WebMvcConfigurer {
//
//     @Override
//     public void addCorsMappings(CorsRegistry registry) {
//         registry.addMapping("/**")
//                 .allowedOriginPatterns("*")
//                 .allowedHeaders(CorsConfiguration.ALL)
//                 .allowedMethods(CorsConfiguration.ALL)
//                 .allowCredentials(true)
//                 //一小时无需再次检测
//                 .maxAge(3600);
//     }
// }
