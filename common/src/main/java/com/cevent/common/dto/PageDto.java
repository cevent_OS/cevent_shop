package com.cevent.common.dto;

import java.util.List;

/**
 * @author cevent
 * @create 2025/1/3 2:39
 * 分页DTO
 *  修饰符说明：
 * private:被private修饰的成员，只能在定义它们的类内部访问，即使继承该类的子类，也无法访问该成员
 * protected：可以在类内部，同一包中的其他类以及任何子类，用于在基类和子类之间共享数据，提供在继承体系中的可见性
 *
 * 先转stringBuffer，再转toString说明：提高性能
 * StringBuffer是一个可变的字符串序列，当需要频繁修改字符串内容时，使用StringBuffer比toString更高效，因为toString是不可变的
 * StringBuffer转toString，实际上是将StringBuffer转为String，因为StringBuffer到String的转换通常是一次性的，转换后的字符串一般不做修改
 * 因此，这种方式相当于直接创建一个string对象，可以节省内存和提高性能
 */
public class PageDto<T> {
    //当前页面
    protected int page;
    //每页条数
    protected int size;
    //总条数(pageInfo处理的total属性为long)
    protected long total;
    //数据，泛型返回不同dto
    protected List<T> list;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    // @Override
    // public String toString() {
    //     return "PageDto{" +
    //             "page=" + page +
    //             ", size=" + size +
    //             ", total=" + total +
    //             ", list=" + list +
    //             '}';
    // }

    @Override
    public String toString(){
        final StringBuffer stringBuffer=new StringBuffer("PageDto{");
        stringBuffer.append("page=").append(page);
        stringBuffer.append(",size=").append(size);
        stringBuffer.append(",total=").append(total);
        stringBuffer.append(",list=").append(list);
        stringBuffer.append("}");
        return stringBuffer.toString();
    }
}
