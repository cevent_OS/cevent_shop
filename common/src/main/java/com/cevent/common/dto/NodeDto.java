package com.cevent.common.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cevent
 * @create 2025/2/2 21:23
 */
public class NodeDto<T> {
    private T data;
    private List<NodeDto<T>> children;

    public NodeDto(T data) {
        this.data = data;
        this.children = new ArrayList<>();
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<NodeDto<T>> getChildren() {
        return children;
    }

    public void setChildren(List<NodeDto<T>> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("NodeDto{");
        sb.append("data=").append(data);
        sb.append(", children=").append(children);
        sb.append('}');
        return sb.toString();
    }
}
