package com.cevent.common.dto;

        import java.math.BigDecimal;
        import java.util.Date;
        import com.fasterxml.jackson.annotation.JsonFormat;



/**
 * @author cevent
 * @create 2025-03-16 23:57:40
 * @description 模块基础表
 */
public class ModuleBaseDto {

    //唯一ID
    private String uniId;
    //模块名称
    private String name;
    //SEO标题
    private String seoTitle;
    //SEO关键字
    private String seoKeyword;
    //SEO描述
    private String seoDescription;
    //模块内容|富文本
    private String content;
    //图片列表
    private String picList;
    //手机端图片列表
    private String miniPicList;
    //核心内容图标
    private String iconList;
    //核心模块描述|{title,desc,icon}
    private String coreDesc;
    //父ID
    private String parentId;
    //模块封面
    private String picCover;
    //时长|单位秒
    private Integer videoTime;
    //模块分类
    private String categoryId;
    //公司介绍分类
    private String comType;
    //股份占比
    private BigDecimal stockHolderRate;
    //内容推送|R广告推送D默认模块
    private String contentRecommend;
    //视频分类
    private String videoType;
    //模块视频
    private String video;
    //视频封面
    private String videoCover;
    //视频标题
    private String videoTitle;
    //视频描述
    private String videoDesc;
    //是否收费||C收费|F免费
    private String videoCharge;
    //PPT封面
    private String pptCover;
    //PPT标题
    private String pptTitle;
    //PPT描述
    private String pptDesc;
    //PPT课件
    private String pptFile;
    //设计原稿样片
    private String designCover;
    //设计原稿名称
    private String designTitle;
    //设计原稿描述
    private String designDesc;
    //设计原稿文件|zip压缩包
    private String designFile;
    //模块路径
    private String modulePath;
    //引用模板路径
    private String templatePath;
    //显示1 || 不显示0
    private Integer isShow;
    //排序
    private Integer sort;
    //创建时间
        @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    //修改时间
        @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;
    //站点ID
    private String domainId;

    public String getUniId(){
        return uniId;
    }
    public void setUniId(String uniId){
        this.uniId=uniId;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getSeoTitle(){
        return seoTitle;
    }
    public void setSeoTitle(String seoTitle){
        this.seoTitle=seoTitle;
    }
    public String getSeoKeyword(){
        return seoKeyword;
    }
    public void setSeoKeyword(String seoKeyword){
        this.seoKeyword=seoKeyword;
    }
    public String getSeoDescription(){
        return seoDescription;
    }
    public void setSeoDescription(String seoDescription){
        this.seoDescription=seoDescription;
    }
    public String getContent(){
        return content;
    }
    public void setContent(String content){
        this.content=content;
    }
    public String getPicList(){
        return picList;
    }
    public void setPicList(String picList){
        this.picList=picList;
    }
    public String getMiniPicList(){
        return miniPicList;
    }
    public void setMiniPicList(String miniPicList){
        this.miniPicList=miniPicList;
    }
    public String getIconList(){
        return iconList;
    }
    public void setIconList(String iconList){
        this.iconList=iconList;
    }
    public String getCoreDesc(){
        return coreDesc;
    }
    public void setCoreDesc(String coreDesc){
        this.coreDesc=coreDesc;
    }
    public String getParentId(){
        return parentId;
    }
    public void setParentId(String parentId){
        this.parentId=parentId;
    }
    public String getPicCover(){
        return picCover;
    }
    public void setPicCover(String picCover){
        this.picCover=picCover;
    }
    public Integer getVideoTime(){
        return videoTime;
    }
    public void setVideoTime(Integer videoTime){
        this.videoTime=videoTime;
    }
    public String getCategoryId(){
        return categoryId;
    }
    public void setCategoryId(String categoryId){
        this.categoryId=categoryId;
    }
    public String getComType(){
        return comType;
    }
    public void setComType(String comType){
        this.comType=comType;
    }
    public BigDecimal getStockHolderRate(){
        return stockHolderRate;
    }
    public void setStockHolderRate(BigDecimal stockHolderRate){
        this.stockHolderRate=stockHolderRate;
    }
    public String getContentRecommend(){
        return contentRecommend;
    }
    public void setContentRecommend(String contentRecommend){
        this.contentRecommend=contentRecommend;
    }
    public String getVideoType(){
        return videoType;
    }
    public void setVideoType(String videoType){
        this.videoType=videoType;
    }
    public String getVideo(){
        return video;
    }
    public void setVideo(String video){
        this.video=video;
    }
    public String getVideoCover(){
        return videoCover;
    }
    public void setVideoCover(String videoCover){
        this.videoCover=videoCover;
    }
    public String getVideoTitle(){
        return videoTitle;
    }
    public void setVideoTitle(String videoTitle){
        this.videoTitle=videoTitle;
    }
    public String getVideoDesc(){
        return videoDesc;
    }
    public void setVideoDesc(String videoDesc){
        this.videoDesc=videoDesc;
    }
    public String getVideoCharge(){
        return videoCharge;
    }
    public void setVideoCharge(String videoCharge){
        this.videoCharge=videoCharge;
    }
    public String getPptCover(){
        return pptCover;
    }
    public void setPptCover(String pptCover){
        this.pptCover=pptCover;
    }
    public String getPptTitle(){
        return pptTitle;
    }
    public void setPptTitle(String pptTitle){
        this.pptTitle=pptTitle;
    }
    public String getPptDesc(){
        return pptDesc;
    }
    public void setPptDesc(String pptDesc){
        this.pptDesc=pptDesc;
    }
    public String getPptFile(){
        return pptFile;
    }
    public void setPptFile(String pptFile){
        this.pptFile=pptFile;
    }
    public String getDesignCover(){
        return designCover;
    }
    public void setDesignCover(String designCover){
        this.designCover=designCover;
    }
    public String getDesignTitle(){
        return designTitle;
    }
    public void setDesignTitle(String designTitle){
        this.designTitle=designTitle;
    }
    public String getDesignDesc(){
        return designDesc;
    }
    public void setDesignDesc(String designDesc){
        this.designDesc=designDesc;
    }
    public String getDesignFile(){
        return designFile;
    }
    public void setDesignFile(String designFile){
        this.designFile=designFile;
    }
    public String getModulePath(){
        return modulePath;
    }
    public void setModulePath(String modulePath){
        this.modulePath=modulePath;
    }
    public String getTemplatePath(){
        return templatePath;
    }
    public void setTemplatePath(String templatePath){
        this.templatePath=templatePath;
    }
    public Integer getIsShow(){
        return isShow;
    }
    public void setIsShow(Integer isShow){
        this.isShow=isShow;
    }
    public Integer getSort(){
        return sort;
    }
    public void setSort(Integer sort){
        this.sort=sort;
    }
    public Date getCreateTime(){
        return createTime;
    }
    public void setCreateTime(Date createTime){
        this.createTime=createTime;
    }
    public Date getUpdateTime(){
        return updateTime;
    }
    public void setUpdateTime(Date updateTime){
        this.updateTime=updateTime;
    }
    public String getDomainId(){
        return domainId;
    }
    public void setDomainId(String domainId){
        this.domainId=domainId;
    }

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [ ");
        sb.append("Hash = ").append(hashCode());

            sb.append(", uniId = ").append(uniId);
            sb.append(", name = ").append(name);
            sb.append(", seoTitle = ").append(seoTitle);
            sb.append(", seoKeyword = ").append(seoKeyword);
            sb.append(", seoDescription = ").append(seoDescription);
            sb.append(", content = ").append(content);
            sb.append(", picList = ").append(picList);
            sb.append(", miniPicList = ").append(miniPicList);
            sb.append(", iconList = ").append(iconList);
            sb.append(", coreDesc = ").append(coreDesc);
            sb.append(", parentId = ").append(parentId);
            sb.append(", picCover = ").append(picCover);
            sb.append(", videoTime = ").append(videoTime);
            sb.append(", categoryId = ").append(categoryId);
            sb.append(", comType = ").append(comType);
            sb.append(", stockHolderRate = ").append(stockHolderRate);
            sb.append(", contentRecommend = ").append(contentRecommend);
            sb.append(", videoType = ").append(videoType);
            sb.append(", video = ").append(video);
            sb.append(", videoCover = ").append(videoCover);
            sb.append(", videoTitle = ").append(videoTitle);
            sb.append(", videoDesc = ").append(videoDesc);
            sb.append(", videoCharge = ").append(videoCharge);
            sb.append(", pptCover = ").append(pptCover);
            sb.append(", pptTitle = ").append(pptTitle);
            sb.append(", pptDesc = ").append(pptDesc);
            sb.append(", pptFile = ").append(pptFile);
            sb.append(", designCover = ").append(designCover);
            sb.append(", designTitle = ").append(designTitle);
            sb.append(", designDesc = ").append(designDesc);
            sb.append(", designFile = ").append(designFile);
            sb.append(", modulePath = ").append(modulePath);
            sb.append(", templatePath = ").append(templatePath);
            sb.append(", isShow = ").append(isShow);
            sb.append(", sort = ").append(sort);
            sb.append(", createTime = ").append(createTime);
            sb.append(", updateTime = ").append(updateTime);
            sb.append(", domainId = ").append(domainId);

        sb.append(" ] ");
        return sb.toString();
    }

}
