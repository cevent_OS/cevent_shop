package com.cevent.common.dto;

import java.util.Date;

/**
 * @author cevent
 * @create 2024/12/25 15:26
 * 数据传输层：模块分类
 * domain持久层不允许修改，如果要修改持久层参数，使用dto数据传输层
 */
public class ModuleCategoryDto {
    private String uniId;

    private Integer sort;

    private String name;

    private Integer level;

    private Integer isShow;

    private String parentId;

    private String articleUniId;

    private String productUniId;

    private String domainId;

    private Date createTime;

    private Date updateTime;

    public String getUniId() {
        return uniId;
    }

    public void setUniId(String uniId) {
        this.uniId = uniId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getArticleUniId() {
        return articleUniId;
    }

    public void setArticleUniId(String articleUniId) {
        this.articleUniId = articleUniId;
    }

    public String getProductUniId() {
        return productUniId;
    }

    public void setProductUniId(String productUniId) {
        this.productUniId = productUniId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        final StringBuffer sb=new StringBuffer("ModuleCategoryDto{");
        sb.append("uniId=").append(uniId);
        sb.append(",sort=").append(sort);
        sb.append(",name=").append(name);
        sb.append(",level=").append(level);
        sb.append(",isShow=").append(isShow);
        sb.append(",parentId=").append(parentId);
        sb.append(",articleUniId=").append(articleUniId);
        sb.append(",productUniId=").append(productUniId);
        sb.append(",domainId=").append(domainId);
        sb.append(",createTime=").append(createTime);
        sb.append(",updateTime=").append(updateTime);
        sb.append("}");
        // return "ModuleCategoryDto{" +
        //         "uniId='" + uniId + '\'' +
        //         ", sort=" + sort +
        //         ", name='" + name + '\'' +
        //         ", level=" + level +
        //         ", isShow=" + isShow +
        //         ", parentId='" + parentId + '\'' +
        //         ", articleUniId='" + articleUniId + '\'' +
        //         ", productUniId='" + productUniId + '\'' +
        //         ", domainId='" + domainId + '\'' +
        //         ", createTime='" + createTime + '\'' +
        //         ", updateTime='" + updateTime + '\'' +
        //         '}';
        return sb.toString();
    }
}
