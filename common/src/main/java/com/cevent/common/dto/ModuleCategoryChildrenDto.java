package com.cevent.common.dto;

import java.util.List;

/**
 * @author cevent
 * @create 2025/1/30 19:54
 */
public class ModuleCategoryChildrenDto extends ModuleCategoryDto{
    private List<ModuleCategoryChildrenDto> children;

    public List<ModuleCategoryChildrenDto> getChildren() {
        return children;
    }

    public void setChildren(List<ModuleCategoryChildrenDto> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ModuleCategoryChildrenDto{");
        sb.append("children=").append(children);
        sb.append('}');
        return sb.toString();
    }
}
