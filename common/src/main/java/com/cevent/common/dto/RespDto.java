package com.cevent.common.dto;

import java.util.List;

/**
 * @author cevent
 * @create 2025/1/8 12:59
 */
public class RespDto<T> {
    //是否成功
    private boolean success=true;
    //返回码
    private String code;
    //返回信息
    private String message;
    //自定义返回内容
    private T content;
    //数组
    private List<T> contentList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public List<T> getContentList() {
        return contentList;
    }

    public void setContentList(List<T> contentList) {
        this.contentList = contentList;
    }

    @Override
    public String toString() {
        final StringBuffer sb=new StringBuffer("RespDto{");
        sb.append("success=").append(success);
        sb.append(",code=").append(code);
        sb.append(",message=").append(message);
        sb.append(",content=").append(content);
        sb.append(",contentList=").append(contentList);
        sb.append("}");
        return sb.toString();
    }
}
