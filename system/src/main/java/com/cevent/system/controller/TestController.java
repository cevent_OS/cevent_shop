package com.cevent.system.controller;

import com.cevent.common.domain.Test;
import com.cevent.common.service.TestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author cevent
 * @create 2024/12/3 14:37
 */
@RestController
public class TestController {

    public static final String SERVER_NAME="系统服务-测试Controller";

    @Resource
    private TestService testService;

    @RequestMapping("/test")
    public String test(){
        return "system interface test by 刘红飞";
    }

    @RequestMapping("/getTestList")
    public List<Test> getTestList(){
        return testService.getList();
    }

    @RequestMapping("/getListByDesc")
    public List<Test> getListByDesc(){
        return testService.getListByDesc();
    }

    @GetMapping("/getById/{id}")
    public List<Test> getById(@PathVariable("id") String id){
        return testService.getById(id);
    }
}
