package com.cevent.cms.controller.admin;

import com.cevent.common.domain.ModuleCategory;
import com.cevent.common.dto.ModuleCategoryChildrenDto;
import com.cevent.common.dto.ModuleCategoryDto;
import com.cevent.common.dto.PageDto;
import com.cevent.common.dto.RespDto;
import com.cevent.common.exception.ValidatorException;
import com.cevent.common.service.ModuleCategoryService;
import com.cevent.common.util.UuidUtil;
import com.cevent.common.util.ValidatorUtil;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author cevent
 * @create 2024/12/25 14:38
 */
@RestController
@RequestMapping("/admin/moduleCategory")
public class ModuleCategoryController {

    public static final String SERVER_NAME="模块分类设置";

    // 快捷键logf
    private static final Logger LOG = LoggerFactory.getLogger(ModuleCategoryController.class);

    @Resource
    private ModuleCategoryService moduleCategoryService;

    /**
     * 分页配置
     *
     * @param pageDto 接受表单方式
     * @param domain  配置header可以没有参数，required=false
     * @return
     */
    @PostMapping("/pageData")
    public RespDto getList(@RequestBody PageDto pageDto, @RequestHeader(value = "domain", required = false) String domain) {
        LOG.info("pageDto: {},domain: {}", pageDto, domain);
        moduleCategoryService.getList(pageDto, domain);
        RespDto respDto = new RespDto();
        respDto.setContent(pageDto);
        return respDto;
    }
    
    @RequestMapping("/tree")
    public RespDto getTree(@RequestHeader(value = "domain",required = false) String domain){
        List<ModuleCategoryChildrenDto> categoryChildrenDtos=moduleCategoryService.getTree(domain);

        RespDto respDto=new RespDto();
        if(categoryChildrenDtos.size()>0){
            respDto.setContent(categoryChildrenDtos);
        }else{
            respDto.setSuccess(false);
            respDto.setMessage("暂无分类数据，请联系后台进行维护");
        }

        return respDto;
    }

    @PostMapping("/save")
    public RespDto save(@RequestBody ModuleCategoryDto moduleCategoryDto, @RequestHeader(value = "domain", required = false) String domain) {
        RespDto respDto = new RespDto();

        ValidatorUtil.isEmpty(moduleCategoryDto.getParentId(), "父级ID");
        ValidatorUtil.isEmpty(domain, "域名");
        ValidatorUtil.isEmpty(moduleCategoryDto.getLevel(), "模块等级");
        ValidatorUtil.isEmpty(moduleCategoryDto.getSort(), "排序");
        ValidatorUtil.isEmpty(moduleCategoryDto.getIsShow(), "是否显示");


        moduleCategoryService.save(moduleCategoryDto, domain);

        respDto.setContent(moduleCategoryDto);
        return respDto;
    }

    @PostMapping("saveTree")
    public RespDto saveTree(@RequestBody List<ModuleCategoryChildrenDto> childrenDtos,@RequestHeader(value = "domain",required = false) String domian){

        RespDto respDto=new RespDto();

        respDto.setContent(moduleCategoryService.saveTree(childrenDtos,domian));

        return respDto;
    }

    @GetMapping("/detail/{uniId}")
    public RespDto detail(@PathVariable String uniId) {
        ModuleCategoryDto moduleCategoryDto = moduleCategoryService.detail(uniId);
        RespDto respDto = new RespDto();
        if (moduleCategoryDto == null) {
            respDto.setSuccess(false);
        }
        respDto.setContent(moduleCategoryDto);
        return respDto;
    }

    @DeleteMapping("/delete/{uniId}")
    public RespDto delete(@PathVariable String uniId, @RequestHeader(value = "domain", required = false) String domain) {
        Boolean flag = moduleCategoryService.delete(uniId, domain);
        RespDto respDto = new RespDto();
        respDto.setSuccess(flag);
        return respDto;
    }

}
