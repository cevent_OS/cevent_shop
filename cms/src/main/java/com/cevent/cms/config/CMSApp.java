package com.cevent.cms.config;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

/**
 * Hello world!
 */
@SpringBootApplication
@EnableEurekaClient
@ComponentScan("com.cevent")
@MapperScan("com.cevent.common.mapper")
public class CMSApp {

    private static final Logger LOG= LoggerFactory.getLogger(CMSApp.class);

    public static void main(String[] args) {
        SpringApplication app=new SpringApplication(CMSApp.class);
        Environment env=app.run(args).getEnvironment();

        LOG.info("启动成功");
        LOG.info("CMS 启动地址：\t http://127.0.0.1:{}",env.getProperty("server.port"));
    }
}
