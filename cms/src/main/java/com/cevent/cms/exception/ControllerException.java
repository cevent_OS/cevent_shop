package com.cevent.cms.exception;

import com.cevent.common.dto.RespDto;
import com.cevent.common.exception.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author cevent
 * @create 2025/1/10 3:04
 * controllerAdvice：是controller增强器，可以对controller做统一处理，如：异常处理、数据处理
 */
@ControllerAdvice
public class ControllerException {

    public static final Logger LOG= LoggerFactory.getLogger(ControllerException.class);

    //只要调用ValidatorUtil，激活了runTime异常（ValidatorException继承了runTime异常），则会被该代码块拦截
    @ExceptionHandler(value=ValidatorException.class)
    @ResponseBody
    public RespDto validatorExceptionHandler(ValidatorException v){
        RespDto respDto=new RespDto();
        respDto.setSuccess(false);
        LOG.warn(v.getMessage());
        respDto.setMessage("请求参数异常！");
        return respDto;
    }
}
